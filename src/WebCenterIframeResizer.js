import ResizeObserver from 'resize-observer-polyfill';

const WebCenterIframeResizer = {
    frameId: null,
    source: null,
    origin: null,
    margin: 20,
    timeoutRunning: false,
    targetHeight: null,
    currentHeight: 0,
    disableSteps: false,
    timeout: 25,
    step: 25,
    logging: false,
    init(margin = 20, log = false) {
        this.logging = log;
        //run only in iframe
        if (window === window.top) {
            return;
        }
        //ResizeObserver
        new ResizeObserver(() => {
            this.log('document changed, resize triggered');
            this.resizeFrame();
        }).observe(document.documentElement);
        //margin
        this.margin = margin;
        //mobile - no steps and longer timeout
        if (document.documentElement.clientWidth < 600) {
            this.disableSteps = true;
            this.timeout = 50;
        }
        //browser compatibility
        let eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
        let eventer = window[eventMethod];
        let messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';
        //wait for message with iframe id
        eventer(messageEvent, event => {
            let data = (window.iframeEvent) ? window.iframeEvent.data : event.data;
            if (typeof data !== 'string' || !/height-\d+/.test(data)) {
                return;
            }
            data = data.split('-');
            this.frameId = parseInt(data[1]);
            this.log(`message recieved, frame-id: ${this.frameId}`);
            this.source = (window.iframeEvent) ? window.iframeEvent.source : event.source;
            this.origin = (window.iframeEvent) ? window.iframeEvent.origin : event.origin;
        });
    },
    resizeFrame(top = false) {
        //no message from parent frame yet - try again shortly
        if (this.frameId === null || this.source === null || this.origin === null) {
            setTimeout(() => {
                this.resizeFrame(top);
            }, 50);
            return;
        }
        //send scroll message immediately
        if (top !== false) {
            let message = this.frameId + '-' + this.currentHeight + '-' + top;
            this.log(`sending message: ${message}`);
            this.source.postMessage(message, this.origin);
        }
        //update targetHeight
        this.targetHeight = document.body.offsetHeight + this.margin;
        //just once every 'timeout' ms
        if (this.timeoutRunning) {
            return;
        }
        //run in timeout
        this.timeoutRunning = true;
        setTimeout(() => {
            if (this.currentHeight === this.targetHeight) {
                this.timeoutRunning = false;
                return; //the end
            }
            let newHeight = this.currentHeight;
            //steps or no steps
            if (this.disableSteps) {
                newHeight = this.targetHeight;
            } else {
                //shrink
                if (this.currentHeight < this.targetHeight) {
                    if (this.targetHeight - this.currentHeight >= this.step) {
                        newHeight += this.step;
                    } else {
                        newHeight = this.targetHeight;
                    }
                }
                //grow
                if (this.currentHeight > this.targetHeight) {
                    if (this.currentHeight - this.targetHeight >= this.step) {
                        newHeight -= this.step;
                    } else {
                        newHeight = this.targetHeight;
                    }
                }
            }
            //send message to parent frame
            let message = this.frameId + '-' + newHeight;
            this.currentHeight = newHeight;
            this.log(`sending message: ${message}`);
            this.source.postMessage(message, this.origin);
            this.timeoutRunning = false;
            this.resizeFrame();
        }, this.timeout);
    },
    log(message) {
      if(this.logging) {
        console.log(`iframe-resizer || ${message}`);
      }
    }
};

export default WebCenterIframeResizer;
