"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _resizeObserverPolyfill = _interopRequireDefault(require("resize-observer-polyfill"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var WebCenterIframeResizer = {
  frameId: null,
  source: null,
  origin: null,
  margin: 20,
  timeoutRunning: false,
  targetHeight: null,
  currentHeight: 0,
  disableSteps: false,
  timeout: 25,
  step: 25,
  logging: false,
  init: function init() {
    var _this = this;
    var margin = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 20;
    var log = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    this.logging = log;
    //run only in iframe
    if (window === window.top) {
      return;
    }
    //ResizeObserver
    new _resizeObserverPolyfill.default(function () {
      _this.log('document changed, resize triggered');
      _this.resizeFrame();
    }).observe(document.documentElement);
    //margin
    this.margin = margin;
    //mobile - no steps and longer timeout
    if (document.documentElement.clientWidth < 600) {
      this.disableSteps = true;
      this.timeout = 50;
    }
    //browser compatibility
    var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
    var eventer = window[eventMethod];
    var messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';
    //wait for message with iframe id
    eventer(messageEvent, function (event) {
      var data = window.iframeEvent ? window.iframeEvent.data : event.data;
      if (typeof data !== 'string' || !/height-\d+/.test(data)) {
        return;
      }
      data = data.split('-');
      _this.frameId = parseInt(data[1]);
      _this.log("message recieved, frame-id: ".concat(_this.frameId));
      _this.source = window.iframeEvent ? window.iframeEvent.source : event.source;
      _this.origin = window.iframeEvent ? window.iframeEvent.origin : event.origin;
    });
  },
  resizeFrame: function resizeFrame() {
    var _this2 = this;
    var top = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    //no message from parent frame yet - try again shortly
    if (this.frameId === null || this.source === null || this.origin === null) {
      setTimeout(function () {
        _this2.resizeFrame(top);
      }, 50);
      return;
    }
    //send scroll message immediately
    if (top !== false) {
      var message = this.frameId + '-' + this.currentHeight + '-' + top;
      this.log("sending message: ".concat(message));
      this.source.postMessage(message, this.origin);
    }
    //update targetHeight
    this.targetHeight = document.body.offsetHeight + this.margin;
    //just once every 'timeout' ms
    if (this.timeoutRunning) {
      return;
    }
    //run in timeout
    this.timeoutRunning = true;
    setTimeout(function () {
      if (_this2.currentHeight === _this2.targetHeight) {
        _this2.timeoutRunning = false;
        return; //the end
      }

      var newHeight = _this2.currentHeight;
      //steps or no steps
      if (_this2.disableSteps) {
        newHeight = _this2.targetHeight;
      } else {
        //shrink
        if (_this2.currentHeight < _this2.targetHeight) {
          if (_this2.targetHeight - _this2.currentHeight >= _this2.step) {
            newHeight += _this2.step;
          } else {
            newHeight = _this2.targetHeight;
          }
        }
        //grow
        if (_this2.currentHeight > _this2.targetHeight) {
          if (_this2.currentHeight - _this2.targetHeight >= _this2.step) {
            newHeight -= _this2.step;
          } else {
            newHeight = _this2.targetHeight;
          }
        }
      }
      //send message to parent frame
      var message = _this2.frameId + '-' + newHeight;
      _this2.currentHeight = newHeight;
      _this2.log("sending message: ".concat(message));
      _this2.source.postMessage(message, _this2.origin);
      _this2.timeoutRunning = false;
      _this2.resizeFrame();
    }, this.timeout);
  },
  log: function log(message) {
    if (this.logging) {
      console.log("iframe-resizer || ".concat(message));
    }
  }
};
var _default = WebCenterIframeResizer;
exports.default = _default;
